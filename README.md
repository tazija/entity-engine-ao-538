## Introduction

This project is Atlassian's fork of the Entity Engine module of the Apache OfBiz project.

## Building

This project requires Maven 3.

## CI

[JBAC](https://jira-bamboo.internal.atlassian.com/browse/OFBEE-ENTITY)

